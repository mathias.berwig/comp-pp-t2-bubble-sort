#include <stdlib.h>
#include <stdio.h>
#include "tempo.h"
#include "omp.h"

#define TAM 5000

int a[TAM];

void inicializa_vetor();
void mostra_vetor();
void ordena();
void mostra_resultado();

// argv[1]: 0 para output completo; 1 para output apenas do tempo em ms
// argv[2]: numero de threads
int main(int argc, char *argv[])
{
	int print = atoi(argv[1]);
	int num_threads = atoi(argv[2]);
	omp_set_num_threads(num_threads);

	if (print == 1) printf("\nInicializando vetor ...\n");

	inicializa_vetor();

	if (print == 1) mostra_vetor();

	tempo1();

	if (print == 1) printf("\nOrdenando vetor ...\n");

	ordena();

	tempo2();

	if (print == 1) mostra_resultado();

	tempoFinal("mili segundos", argv[0], MSGLOG);
}


void ordena()
{	
	int lsup = TAM, aux, cont, cont2, bolha;

	while (lsup > 0) {
		bolha = 0;
		#pragma omp parallel for private(cont) collapse(1)
		for (cont=1; cont<TAM; cont++)
			for (cont2 = cont-1; cont2 >= 0; cont2--)
				if (a[cont] < a[cont2])
				{
					aux = a[cont];
					a[cont] = a[cont2];
					a[cont2] = aux;
					bolha = aux;
				}
		lsup = bolha;
	}
}

void mostra_vetor()
{
	int i;
	printf("\nV E T O R \n");

	for (i=0; i < TAM; i ++)
		printf("%4d", a[i]);

	printf("\n\n\n");
}

void mostra_resultado()
{
	int i;
	printf("\nV E T O R    R E S U L T A D O \n");

	for (i=0; i < TAM; i ++)
		printf("%4d", a[i]);

	printf("\n\n\n");
}

void inicializa_vetor()
{
	int i;
	for (i=0; i < TAM; i ++)
		a[i] = rand() % TAM;
}